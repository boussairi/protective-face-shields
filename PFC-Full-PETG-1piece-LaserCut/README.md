# PETG lasercut face shield

This is a lasercut version of the protective mask fabrication effort at YourLAB (Andenne - Belgium). It is inspired by a design from Doctor Carmen Ionescu and FabLab ULB, and delivered under [CC license](./LICENSE.md).

This design and documentation was written by Nicolas Arnould <nicolas@yourlab.be >;

## Important note

Make sure you have first read the general documentation (folder ahead) about hospital validation and history.

## Objective

To create a simple face shield design made of only one material, easy and fast to cut on a laser cutting machine to perform a high production rate.

## Technology

CO2 laser cutting machine.
Minimum working area size of 660mm by 245mm.

## Material

0.5mm thick PETG sheets.

## Software

The design was drawn using [LibreCAD](https://librecad.org/).
[LibreCAD](https://librecad.org/) is a free open source CAD application for GNU/Linux, Windows, macOS. Please refer to the documentation.

[InkScape] (https://inkscape.org/) was used to convert the .dxf file to .svg and to send the information to the laser cutting machine software.
[InkScape] (https://inkscape.org/) is a professional vector graphics editor for GNU/Linux, Windows and macOS. It's free and open source.

[Gimp] (https://www.gimp.org/) was used to handle the photos.
[Gimp] (https://www.gimp.org/) is a free and open source image editor available for GNU/Linux, Windows and macOS.


## Files

[faceshield_petg_lasercut.dxf](./faceshield_petg_lasercut.dxf)
[faceshield_petg_lasercut.svg](./faceshield_petg_lasercut.svg)

## Cutting issue (Trotec Speedy 400 120W laser - 0.5mm PETG sheet)

Power : 100% ;
Speed : 1.5 ;
Frequency : 1000 Hz

Cuuting time : 20 seconds

## Overview

![](images/faceshield_petg_overview01.JPG)
![](images/faceshield_petg_overview02.JPG)
![](images/faceshield_petg_overview03.jpg)

## Assembly

![](images/faceshield_petg_assembly01.JPG)
![](images/faceshield_petg_assembly02.JPG)
![](images/faceshield_petg_assembly03.JPG)
![](images/faceshield_petg_assembly04.JPG)
![](images/faceshield_petg_assembly05.JPG)
![](images/faceshield_petg_assembly06.JPG)
![](images/faceshield_petg_assembly07.JPG)
![](images/faceshield_petg_assembly08.JPG)
