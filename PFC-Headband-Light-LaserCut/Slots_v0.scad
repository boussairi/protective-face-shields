/*
//* FILE   : anti_projection_2D-slots_v0.scad
//* AUTHOR : Denis Terwagne
//* DATE   : 2020-03-20
//* Version: 0.0 (first draft)
//* Notes  : 
//* License : M.I.T. (https://opensource.org/licenses/MIT)
//*
//*/

//tolerance
t=0;

out_d   = 30; //120;  //inside diameter of outer ring
in_d    = 20; //100;  //inside diameter of inner ring
height  = 10;   //height of the module
th      = 0.8;    //thickness of module
height_slot = height/2;
width_slot = th*3;
length_tab = th*4; //length of the tab
length_piece = 30;

////top piece
//translate([0,height*4,0]){
//
//cube([height/2,height,th]);
//translate([0,height/4,0])cube([length_tab,height_slot-t,th]);
//    
//translate([length_tab,0,0]){
//difference(){
//translate([0,height/2,0])cylinder(th,r=height/2,$fn=100);
//translate([-height/2,0,0])cube([height/2,height,th]);
//}
//}}

projection(cut=true){

//middle piece
translate([0,2*height,0]){

cube([length_piece-height/2-length_tab,height,th]);
translate([0,height/4,0])cube([length_piece-height/2+length_tab,height_slot-t,th]);
    
translate([length_piece-height/2,0,0]){
difference(){
translate([0,height/2,0])
    cylinder(th,r=height/2,$fn=100);
translate([-height/2,0,0])cube([height/2,height,th]);
}
}}

//lower piece
difference(){
cube([length_piece,height,th]);
translate([length_piece/3*2-height+width_slot-t,height/2-width_slot/2,0])cube([height+t,width_slot,th]);     
translate([length_piece/3*2,height/4,0]) cube([width_slot,height_slot+t,th]);    
}

translate([0,-height*2,0]){

difference(){
cube([length_piece,height,th]);
translate([length_piece/3*2-height+width_slot-t,height/2-width_slot/2,0])cube([height+t,width_slot,th]);     
translate([length_piece/3*2,height/4,0]) cube([width_slot,height_slot+t,th]);    
}
}

}