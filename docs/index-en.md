# Protective Face Shields

[Pour le français cliquez ici](index.md)

### How to make Protective Face Shields in Fablabs

A **multidisciplinary** team at the Fablab ULB (Belgium) has developed Face Protective Masks in collaboration with the Hospital CHU Saint-Pierre in Brussels. We are now contacted by several hospitals in Belgium and outside Belgium. We are currently supported by the Fab-C in Charleroi and the Fablab YourLab in Andenne, other Fablabs are joining the operation.

All together, from the 23rd of March 23 to the 10th of April 2020, we have produced more than 30 000 masks in Belgium.

You will find below several tutorials to reproduce these masks. The 4 models on this site have been validated by the CHU Saint-Pierre of Brussels (Belgium).

![](./images/AllMasks.jpg)

## Practical info

### Do you need masks?



For the region of **Charleroi Métropole including Chimay**, please contact Delphine Dauby ([Fab-C](https://www.fablab-charleroi.be/)) at [delphine.dauby@ulb.ac.be](mailto:delphine.dauby@ulb.ac.be) (0479/209347 - [facebook](https://www.facebook.com/notes/fab-c-fablab-charleroi-m%C3%A9tropole/des-visi%C3%A8res-pour-le-personnel-soignant-m%C3%A9dical-social-etc/2544439042483757/)).

For the region of **Mons and surroundings**, please contact Martin Waroux ([FabLabMons](https://fablabmons.be/)) at [martin.waroux@fablabmons.be](mailto:martin.waroux@fablabmons.be) (0499/253305)

For the region of **Tournai - Wallonie Picarde**, please contact François Bouton ([Fablab WAPI](https://www.fablabwapi.be/)) at [francois.bouton@wapshub.be](mailto:francois.bouton@wapshub.be) (0477/134010)

For the region of **Andenne**, please contact Shirley Lefebvre ([YourLab](https://yourlab.be/)) at [shirley@yourlab.be](mailto:shirley@yourlab.be) (0474/792535)

For **other regions and Brussels**, please contact Cécile Sztalberg of the [Fondation Michel Crémer](https://michelcremerfoundation.eu/) at the address [covid@michelcremerfoundation.eu](mailto:covid@michelcremerfoundation.eu) who supports the operation of the [Fablab ULB](http://fablab-ulb.be/ who).

### Do you want to help us ?

##### You have access to 3D printers or laser cutters to make additional masks

1. Please contact your nearest FabLab :

   * For the region of **Charleroi Métropole including Chimay** :  
 [Fab-C](https://www.fablab-charleroi.be/) ([delphine.dauby@ulb.ac.be](mailto:delphine.dauby@ulb.ac.be) - 0479/209347 - [facebook](https://www.facebook.com/notes/fab-c-fablab-charleroi-m%C3%A9tropole/des-visi%C3%A8res-pour-le-personnel-soignant-m%C3%A9dical-social-etc/2544439042483757/))
   * For the region of **Mons and surroundings** :  
 [FabLabMons](https://fablabmons.be/) ([martin.waroux@fablabmons.be](mailto:martin.waroux@fablabmons.be) - 0499/253305)
   * For the region of **Tournai - Wallonia Picardy** :  
 [Fablab WAPI](https://www.fablabwapi.be/) ([francois.bouton@wapshub.be](mailto:francois.bouton@wapshub.be) - 0477/134010)
   * For the region of **Namur** :  
[Trakk - Fablab](https://www.trakk.be/) ([fablab@trakk.be](mailto:fablab@trakk.be))
   * For the region of **Andenne** :  
[YourLab](https://yourlab.be/) ([shirley@yourlab.be](mailto:shirley@yourlab.be) - 0474/792535)
   * For **other regions and Brussels** :  
[Fablab ULB](http://fablab-ulb.be/) ([contact@fablab-ulb.be](mailto:contact@fablab-ulb.be) - [facebook](https://www.facebook.com/fablabULB/))

2. Make the masks by following the tutorials below (these 4 models have been approved by the CHU Saint-Pierre de Bruxelles).

3. The Fablabs will take care of redistributing the masks produced to the hospitals and 1st line staff who need them.

##### The Fablab ULB needs equipment!

Can you help us? Drop off the equipment directly to the Fablab ULB. Are requested :

* 300m x 210mm x 0.2mm PCV A4 plastic sheets (A4 slides)
* A4 gusseted envelopes

## Masks models and tutorials

4 models have been validated by the medical officers of health and the management of the CHU Saint-Pierre hospital in Brussels with a first order of 1500 units to be produced. We share tutorials for the 4 of them here.

### 1 - with the 3D printer

Inspired by the protective mask created by [Prusa](https://www.prusaprinters.org/prints/25857-prusa-protective-face-shield-rc2/files), we have developed a lighter version that is faster to produce due to time constraints.

![](./images/P1-summary.jpeg)

#### Hardware and tools

* Material :
  * a transparent A4 sheet
  * PETG or PLA coil
* Tools:
  * 3D printer
  * punch (5 mm or 6 mm holes)
* Production time: 30 min per mask

#### Tutorial

[Tutorial here](./PFC-Headband-Light-3DPrint/tuto-en.md)

### 2 - with the Laser Cutter

We have had [this model from Thingiverse designed by LadyLibertyHK](http://thingiverse.com/thing:4159366) validated by the CHU Saint-Pierre hospital in Brussels.

![](./images/P2-summary.jpeg)


#### Hardware and tools

* Material
  * Plexiglas plate in 3 mm
  * transparent A4 sheet
* Tools:
  * laser cutting machine
  * one drill + 3.2 mm drill bit.
* Production time: 1 minute per mask

#### Tutorial

[Tutorial here](./PFC-Cap-LaserCut/tuto-en.md)

### 3 - with a flexible strip and a laser cutter

This solution was inspired by a design by Dr. Carmen Ionescu, from the anaesthesia department of the Ixelles hospital, and slightly adapted thanks to feedback from the CHU Saint-Pierre hospital in Brussels. It is very quick to produce and less brittle than the rigid version.

![](./images/headband-flexible-general.png)

#### Hardware and tools

* Material
  * A sheet of transparent A4 PVC.
  * A sheet of Priplak® (polypropylene), 1mm thick, 70 cm long minimum.
* Tools:
  * Laser cutting machine
  * Cutter
* Production time: 2 min/piece

#### Tutorial

The complete tutorial and the files for its realization [are available here](PFC-Headband-Flexible-LaserCut/tuto-en.md).

### 3bis - with a simpler flexible strip and a laser cutter

This solution was inspired by a design by Dr. Carmen Ionescu, from the anaesthesia department of the Ixelles hospital, and shared under a [Creative Commons BY-NC-SA 3.0 license](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/). It is very fast to produce and less brittle than the rigid version.

**This version is an alternative of the flexible headband to laser cutting.**

![](PFC-Headband-Flexible-LaserCut-Simple/images/general.png)


#### Hardware and tools
* Hardware:
  * A sheet of transparent A4 PVC.
  * A sheet of Priplak® (polypropylene), 1mm thick, minimum length 70 cm.
* Tools:
  * Laser cutting machine
  * Cutter
* Production time: 1min30/piece


#### Tutorial

The complete tutorial and the files for its realization [are available here](PFC-Headband-Flexible-LaserCut-Simple/tuto-en.md).

### 3ter - with a flexible strip and a cutter only


This solution was inspired by a design by Dr. Carmen Ionescu, from the anaesthesia department of the Ixelles hospital, and shared under a [Creative Commons BY-NC-SA 3.0 license](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/).


This is an alternative version of the flexible headband made with the laser cutter.


![](PFC-Headband-Flexible-Cutter/files/headband-flexible-cutter-general.jpg)

It is very fast to produce and does not require any technological cutting machine.

#### Hardware and tools

* Hardware:
  * A sheet of transparent A4 PVC.
  * A sheet of Priplak® (polypropylene), 1mm thick, minimum length 70 cm.
* Tools:
  * Cutter or a pair of scissors
* Production time: 5min/piece


#### Tutorial

The complete tutorial [is available here](PFC-Headband-Flexible-Cutter/tuto-en.md).


### 4 - with a hygiene cap

simple solution from the Department of Stomatology and Maxillofacial Surgery of the St-Pierre University Hospital in Brussels and from the Fablab ULB.

![](./images/P4-Hygiene_cap.jpeg)

#### Hardware and tools

* hardware
  * A Hygiene cap
  * A sheet of transparent A4 plastic.
* Tools:
  * Stapler
* Production time: 1 min/piece

#### Tutorial

The complete tutorial [is available here](PFC-Hygiene-Cap/tuto-en.md).

## Mask disinfection protocol


---

> ***[Download here a disinfection protocol made with the CHU Saint-Pierre of Brussels](Hygiene/Cleaning-protocol-en.pdf)***

---



More information [on this page](Hygiene/cleaning-en.md).


## About Us

The [Fablab ULB](http://fablab-ulb.be/) (Université Libre de Bruxelles ULB, Belgium) is a multidisciplinary team of academics, researchers, technicians, students from different Faculties (Science, Architecture, Law, Brussels Polytechnic) and also artists, designers, makers and citizens. It is supported in its mission by various ULB research laboratories, namely the Frugal LAB (Faculty of Sciences), the Juris LAB (Faculty of Law) and the BEAMS (Ecole Polytechnique de Bruxelles).

## With the precious collaboration of

This particular project was carried out in collaboration with the [Fab-C](https://www.fablab-charleroi.be) of Charleroi and the fablab [YourLab](https://yourlab.be/) of Andenne and supported by the [fondation Michel Crémer](https://michelcremerfoundation.eu/).

We have now been joined by other fablabs, associations and companies:

* [FabLabMons](https://fablabmons.be/)
* [Fablab WAPI](https://www.fablabwapi.be/)
* [ULB Physics Experimentarium (Faculty of Science)](http://www.experimentarium.be/)
* [ISIB](https://www.he2b.be/campus-isib)
* [A2RC Architectural Office](https://www.a2rc.be/)
* [Jaspers-Eyers Architectural Office](https://www.jaspers-eyers.be/)
* [Elysta](https://elysta.be/)
* [City Fab 2 (Brussels)](https://www.citydev.brussels/fr/projets/cityfab-2)
* Melt+
* and many individuals (list to come)

## They talk about us

In these times of crisis, journalists sometimes take unfortunate shortcuts:  
In the articles below, **understand by "engineers" -> a "multidisciplinary team of scientists, architects, designers, engineers, doctors and technicians "**.  
Multidisciplinarity is the DNA of Fablab!

* [RTBF JT du 19h30, le 23 mars 2020](https://michelcremerfoundation.eu/wp-content/uploads/2020/03/Les-ing%C3%A9nieurs-ont-mis-au-point-des-surmasques.mp4)
* [Actus ULB, 25 mars 2020](https://actus.ulb.be/fr/actus/institution-et-engagements/des-visieres-pour-proteger-nos-soignants)
* [Journal La Libre Belgique, 26 mars 2020](https://www.lalibre.be/planete/sante/les-fablabs-developpent-un-surmasque-a-destination-du-personnel-medical-5e7b6b4e7b50a6162bbc5af3)
* [Presentation of the masks operation to the worldwide Fablab Network through Fab Academy (10 first minutes)](https://vimeo.com/402353963)
