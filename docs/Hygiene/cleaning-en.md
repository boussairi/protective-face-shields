# Protective Face Shields Cleaning Protocol

---

> ***[Download here a disinfection protocol made with the CHU Saint-Pierre of Brussels](Cleaning-protocol-en.pdf)***

---

Protective Face Shields are an added protection **in addition to your validated mask**, and that helps protect these masks. They consist of **a rigid support and a visor: a flexible sheet of PVC (Polyvinyl Chloride).**

> ***This facial protection should be cleaned as soon as it is removed.***

**ATTENTION :**

* Protective Face Shields do not tolerate a temperature above 50 degrees Celsius
* Protective Face Shields are not resistant to acetone
* Protective Face Shields are not resistant to chloroform

![](images/hygiene-mask.jpg)

1. Remove the visor from its support
2. Lay the visor flat
3. Clean both sides, the support, and the insertion holes on the support :
✓Surface disinfectant spray (ex: Anios Surfa'safe ™, Incidin ™, Clinell ™, ...)
4. Do not rinse to ensure sufficient contact between the virucide (disinfectant) and the visor.
5. Replace the visor on the support.



**Protocol to be validated by the competent authorities of each institution. The Fablab ULB does not take any responsibility in case of problems.**



## Chemical compatibility of plastics and other resources

* More information on the disinfection of 3D printed masks can be found on [the Prusa website](https://help.prusa3d.com/en/article/prusa-face-shield-disinfection_125457)
* Surface spray used in hospital to disinfect (virucide, bactericide):
   * [Clinell®](https://www.nu-careproducts.co.uk/brands/clinell.htm)
   * [Anios Surfa’safe®](https://sds.rentokil-initial.com/sds/files/GP-Rentokil-Surfasafe_FICHES_TECHNIQUES-FR-SDSxxxx_01_Technical_Sheet.pdf)
   * [Incidin®](https://www.verpa.be/content/files/artikelen/TECFR/3025780_TEC_FR.pdf)

* The table below indicates the compatibility of different materials used for the protective face shields with typical solvents (**R: good resistance**, **L: resistance but limited**, **D: degradation**).

|| [PVC](https://www.gilsoneng.com/reference/ChemRes.pdf) | [PLA](https://www.researchgate.net/figure/Map-of-chemical-compatibility-of-plastics-and-3-D-printing-materials-from-the-literature_fig2_326697946) |	ABS     ([1](http://www.kelco.com.au/wp-content/uploads/2009/02/abs-chemical-compatibility-guide.pdf), [2](https://www.plasticsintl.com/chemical-resistance-chart), [3](https://www.gilsoneng.com/reference/ChemRes.pdf))|	[Plexi / PMMA](http://tools.thermofisher.com/content/sfs/brochures/D20480.pdf) | [Priplak/  Polypropylène (PP)](https://www.gilsoneng.com/reference/ChemRes.pdf)
|---|---|---|---|---|---|
|Ethanol |R| *L* |R| ***D*** |R|
|Isopropanol/2-propanol/Isopropyl alcohol	|R|R| *L* | ***D*** |R|
| 1-propanol|R|R| *L* ||R|
| Acetone	| ***D*** | ***D*** | ***D*** | ***D*** |R|
