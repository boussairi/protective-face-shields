# 3bis - Solution en bandeau flexible simple – 1 lanière de 70cm


Cette solution a été inspirée d’un design de Dr Carmen Ionescu, du service d'anesthésie de l'hopital d'Ixelles, et partagée sous une [licence Creative Commons BY-NC-SA 3.0](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/). Elle est très rapide à produire et moins cassante que la version rigide.

**Cette version est une alternative du [bandeau flexible à la découpe laser](../PFC-Headband-Flexible-LaserCut/tuto-fr.md).**

![](images/general.png)

## Matériel
*	Matériel :

   * Une feuille de PVC A4 transparente.
   * Une feuille de Priplak® (polypropylene), d’épaisseur 0.8mm, de 70cm de longueur minimum.
* Outils :
  * Découpeuse laser
  * Cutter
*	Temps de production : 1min30/pièce

## Processus de fabrication

# Etape 1 – Découpe de la structure (« bandelette » ci-dessous)


Téléchargez le modèle ci-dessous (.SLDPRT ou .DXF) et effectuez la découpe.

La version "bandelette-simple" a été développée pour toutes les taille de crâne, mais est trop grande pour les machines de 600 mm.
La version "bandelette-simple-small" est pour les machines plus petites (600 mm), avec une inscription "small" dessus.

* [Bandelette-simple.SLDPRT](files/Bandeau-simple-masque-anti-projection-700-2.SLDPRT)
* [Bandelette-simple.dxf](files/Bandeau-simple-masque-anti-projection-700-2.DXF)
* [Bandelette-simple-small.SLDPRT](files/Bandeau-simple-masque-anti-projection-600.SLDPRT)
* [Bandelette-simple-small.dxf](files/Bandeau-simple-masque-anti-projection-600.DXF)

![](images/bandelette-simple.png)

Par exemple, on peut découper 25 bandeaux sur une feuille de 70cm sur 51cm :

![](images/25pcs.png)

* [Format SVG](files/Bandeau-simple-masque-anti-projection-700-25pcs.svg)
* [Format DXF](files/Bandeau-simple-masque-anti-projection-700-25pcs.dxf)

Voici les paramètres utilisés pour une découpeuse laser de 100W, sur une feuille en polypropylene de 0.8mm :

* Vitesse : 2600 mm/s
* Puissance : 80 %

Il est important de vérifier que la longueur de l'entaille corresponde parfaitement à la largeur de la bande découpée au laser (une entaille légèrement inférieure sera préférée à une entaille trop grande, puisque l'ouverture se donnera un petit peu lors de l'insertion de la bande).
C'est cet ajustement qui permet à la feuille transparente de tenir correctement en place et qui assure un frottement suffisant aussi pour ajuster l'écartement entre la feuille transparente et le front de l'utilisateur.

# Etape 2 - Découpez la feuille A4 transparente

Découpez dans la feuille transparente, 4 entailles de 20mm de long situées comme sur le plan ci-dessous (en mm). Il a été constaté qu’une entaille ajustée (de dimensions égales à la largeur des bandes) est nécessaire pour un meilleur maintien de la feuille transparente.


![](images/cut1.png)

Pour simplifier les découpes et l’assemblage, une version avec 2 entailles est également possible pour un maintien suffisant du masque :


![](images/cut2.png)

# Etape 3 – Assembler
L’assemblage se fait à l’hôpital, mais est décrit pour information :
1. Insérer la bandelette dans les entailles sur toute la longueur de la feuille


![](images/assembly1.png)

2. L’ajustement de la visière se fait directement sur la tête de l’utilisateur. La visière tient à distance du front par frottement.


![](images/assembly2.jpg)

## Auteurs et remerciements

Tutoriel réalisé par Loïc Blanc et Ramzi Ben Hassen (BEAMS & Fablab ULB). Merci à toute l'équipe pour votre contribution et vos photos.
