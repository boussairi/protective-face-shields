# PETG lasercut face shield

This is a lasercut version of the protective mask fabrication effort at YourLAB (Andenne - Belgium), delivered under [CC license](./LICENSE.md).

This design and documentation was written by Nicolas Arnould < nicolas@yourlab.be >.

## Important note

Make sure you have first read the general documentation (folder ahead) about hospital validation and history.

## Objective

To create a simple face shield design made of only one material, easy and fast to cut on a laser cutting machine to perform a high production rate.

## Technology

CO2 laser cutting machine.
Minimum working area size of 650mm by 245mm.

## Material

0.5mm thick PETG sheets.

## Software

The design was drawn using [LibreCAD](https://librecad.org/).
[LibreCAD](https://librecad.org/) is a free open source CAD application for Linux, Windows, macOS. Please refer to the documentation.

[InkScape] (https://inkscape.org/) was used to convert the .dxf file to .svg and to send the information to the laser cutting machine software.
[InkScape] (https://inkscape.org/) is a professional vector graphics editor for Linux, Windows and macOS. It's free and open source.

## Files

[faceshield_petg_lasercut.dxf](faceshield_petg_lasercut.dxf)
[faceshield_petg_lasercut.svg](faceshield_petg_lasercut.svg)

## Cutting issue (Trotec Speedy 400 120W laser - 0.5mm PETG sheet)

Power : 100% ;
Speed : 1.5 ;
Frequency : 1000 Hz

Cutting time : 65 seconds

## Overview

![](images/faceshield_petg_overview01.jpg)
![](images/faceshield_petg_overview02.jpg)
![](images/faceshield_petg_overview03.jpg)

## Assembly
.JPG
English : [faceshield_petg_assemblyguide_EN.pdf](faceshield_petg_assemblyguide_EN.pdf)
French : [faceshield_petg_assemblyguide_FR.pdf](faceshield_petg_assemblyguide_FR.pdf)
